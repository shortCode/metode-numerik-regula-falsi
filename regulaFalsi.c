#include<stdio.h>
#include<math.h>

double fungsi(double x){
    x = (pow(x,3) - (6*pow(x,2))+ (11 * x) - 6);
    return x;
}

double regulaFalsi(double a, double b){
    double sumbuTengah = ((a*fungsi(b)) - (b*fungsi(a))) / (fungsi(b) - fungsi(a));
    return sumbuTengah;
}

int main(/*fmf*/){system("clear");
    double sumbuAtas = 2.5, sumbuBawah = 3.6, sumbuTengah;
    short count = 0;
    double toleransiErr=0.00001;
    
    printf("Diketahui : \n   fungsi (x) \t\t: ((x^3) - (6*x^2) + 11 - 6) \n   Sumbu Atas\t\t: %f\n   Sumbu Bawah\t\t: %f\n   Toleransi Error\t: %f \n\n", sumbuAtas, sumbuBawah, toleransiErr);
    printf("=====================================================================================================\n");
    printf("   No\t|Sumbu Atas\t|Sumbu Bawah\t|Sumbu Tengah\t|fungsi(A)\t|fungsi(B)\t|fungsi(C)\n");
    printf("=====================================================================================================\n");
    
    while (fabs(fungsi(sumbuTengah))> toleransiErr){
        count ++;
        sumbuTengah = regulaFalsi (sumbuAtas, sumbuBawah);
        printf("   %d\t|%f\t|%f\t|%f\t|%f\t|%f\t|%f\n", count, sumbuAtas, sumbuBawah, sumbuTengah, fungsi(sumbuAtas),fungsi(sumbuBawah), fungsi(sumbuTengah));
        if(fungsi(sumbuTengah)*fungsi(sumbuAtas) > 0){
            sumbuAtas = sumbuTengah;
        }else{
            sumbuBawah = sumbuTengah;
        }
    };
    
    printf("=====================================================================================================\n");
    printf("\nAkar nyata dari f(x) = ((x^3) - (6*x^2) + 11 - 6) adalah %f\n\n", fungsi(sumbuTengah));
    return 0;
}